﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OrfParser
{
    public class Program
    {

        static void Main(string[] args)
        {

        }


        /// <summary>
        /// Parses given orf website and returns list of iptv shows.
        /// </summary>
        /// <param name="addressToParse"></param>
        /// <returns></returns>
        public static List<string> GetOrfIpTvShows(string addressToParse)
        {

            List<string> tmpShows = new List<string>();

            try
            {

                WebClient tmpWebClient = new WebClient();
                HtmlDocument tmpDocument = new HtmlDocument();

                // Get website with webclient.
                tmpWebClient.Encoding = Encoding.UTF8;
                string tmpPage = tmpWebClient.DownloadString(addressToParse);

                // Load website into HtmlAgilityPack document.
                tmpDocument.LoadHtml(tmpPage);

                // Get nodes containing iptv shows from orf website.
                List<HtmlNode> tmpTeaserDivs = tmpDocument.DocumentNode.SelectSingleNode("//div[@class='base_list_wrapper episodes js_extra_content']")
                    .Descendants("li").Where(div => div.HasClass("base_list_item")).ToList();

                // Fill shows list with list element infos.
                foreach (var node in tmpTeaserDivs)
                {

                    string tmpTitle = string.Empty;
                    string tmpLink = string.Empty;

                    // Iterate through shows and filter iptv shows.
                    foreach (var att in node.ChildNodes)
                    {
                        switch (att.Name)
                        {

                            case "a":

                                if (att.Attributes["title"] != null && !att.Attributes["title"].Value.StartsWith("AD |"))
                                {
                                    tmpTitle = att.Attributes["title"]?.Value ?? "Title missing";
                                    tmpLink = att.Attributes["href"]?.Value ?? "URL missing";
                                }
                                
                                break;

                            case "p":

                                tmpLink = att.ChildNodes[0].Attributes["href"]?.Value ?? "URL missing";

                                break;

                            default: break;

                        }

                    }

                    if (!string.IsNullOrWhiteSpace(tmpTitle) && !string.IsNullOrWhiteSpace(tmpLink))
                    {
                        tmpShows.Add(tmpTitle + "_____" + tmpLink);
                    }
                    
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return tmpShows;
        }


    }

}
